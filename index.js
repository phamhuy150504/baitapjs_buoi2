


/**
 * Viết chương trình tính lương nhân viên
    input: lương 1 ngày: 100.000
            Người dùng nhập vào lương 1 ngày:
    output: lương = luong1 ngày * số ngày làm
    progress: tạo 1 function tính lương chạy function đó sau khi người dùng nhấn nút button
            b2: táo biến lấy giá trị của người dùng nhập vào
            b3: tạo 1 biến tính lương = giá trị người dùng nhập vào * 100000
            b4: gán giá trị đó cho thẻ span
 */



function tienLuong() {
    var soNgay = document.getElementById("soNgay");
    console.log(soNgay.value);
    var luong = soNgay.value * 100000;
    var submit = document.getElementById("luong");
    submit.innerHTML = luong.toLocaleString() + " " + "vnd";
}



/**
 * Viết chương trình nhập vào 5 số thực. Tính giá trị tb của 5 số thực xuất ra màn hình
 input: người dùng nhập vào giá trị 5 số sực
 ouput: igas trị trung bình  của 5 số thực đó
 progress: tao ra 5 the input
        - clg lay value
        - lay 5 value + lai / 5 *1 hoac ep kieu bang Number()
        */


function ketQua() {
    var giaTriTB = document.getElementById("giaTriTB");
    console.log(giaTriTB.value);
    var chucNghin = Math.floor(giaTriTB.value / 10000);
    console.log(chucNghin);
    var hangNghin = Math.floor(giaTriTB.value % 10000 / 1000);
    console.log(hangNghin);
    var hangTram = Math.floor(giaTriTB.value % 1000 / 100);
    console.log(hangTram);
    var hangChuc = Math.floor(giaTriTB.value % 100 / 10);
    console.log(hangNghin);
    var hangDonVi = Math.floor(giaTriTB.value % 10);
    console.log(hangDonVi);
    var trungBinhTong = Math.floor(chucNghin + hangNghin + hangTram + hangChuc + hangDonVi) / 5

    var hienThi = document.getElementById("trungbinh");
    hienThi.innerHTML = trungBinhTong;
}

function trungBinhST() {
    var giaTri1 = document.getElementById("so1");
    var giaTri2 = document.getElementById("so2");
    var giaTri3 = document.getElementById("so3");
    var giaTri4 = document.getElementById("so4");
    var giaTri5 = document.getElementById("so5");
    console.log(Number(giaTri1.value));
    console.log(Number(giaTri2.value));
    console.log(Number(giaTri3.value));
    console.log(Number(giaTri4.value));
    console.log(Number(giaTri5.value));

    var ketQua2 = Number(giaTri1.value) + Number(giaTri2.value) + Number(giaTri3.value) + Number(giaTri4.value) + Number(giaTri5.value);
    console.log(ketQua2);
    var trungBinhSoThuc = document.getElementById("ketQuaST");
    trungBinhSoThuc.innerHTML = ketQua2.toLocaleString();
}




// tính USD --> VND 
/**
 * input : 23.500vnd && value $ nguoi dung nhap vao
 * ouput: Xuat ra vnd === $
 * progress: tao slt input cho user nhap value va lay value do * voi 23.500 
 *                  
 */
function quyDoi() {
    var USD = document.getElementById("USD").value;

    VND = USD * 23500;
    console.log(VND)
    var thanhTien = document.getElementById("tienQuyDoi");
    thanhTien.innerHTML = VND.toLocaleString() + " " + "VNĐ";
}



// Chương trình tính chuVi dienTich HCN 
/*
input: chieuDai && chieuROng ( user Nhap )
output: dienTich && chuVi HCN
progress: tao slt input lay value do nguoi dung nhap vao 
        tao bien tinh dienTich && chuVi Hcn = cthuc 
*/

function HCN() {
    var chieuDai = document.getElementById("chieuDai").value;
    var chieuRong = document.getElementById("chieuRong").value;
    var dienTich = chieuDai * chieuRong;
    var chuVi = (Number(chieuDai) + Number(chieuRong)) * 2
    console.log(dienTich);
    console.log(chuVi);
    var ketQuaCV = document.getElementById("chuVi");
    var ketQuaDT = document.getElementById("dienTich");
    ketQuaCV.innerHTML = chuVi;
    ketQuaDT.innerHTML = dienTich;
}

//tong 2 ky so
/**
 * input: 1 so co 2 chu so user nhap
 * out: tong cua 2 ky so do
 * progress: lay so do chia cho 10 dung ham math.floor de lay so nguyen
 * Lay do do % 10 dung ham math.floor lay so nguyen
 */

function tongHaiChuSo() {
    var giaTri_Bai5 = document.getElementById("chuSo").value;
    console.log(giaTri_Bai5);
    var hangChuc_Bai5 = Math.floor(giaTri_Bai5 / 10);
    var hangDV_Bai5 = Math.floor(giaTri_Bai5 % 10);
    console.log(hangChuc_Bai5);
    console.log(hangDV_Bai5);
    var tongHaiChuSo_Bai5 = hangChuc_Bai5 + hangDV_Bai5;
    console.log(tongHaiChuSo_Bai5);
    var tongHaiSo = document.getElementById("tongHaiSo");
    tongHaiSo.innerHTML = tongHaiChuSo_Bai5;

}



var btnDangNhap = document.getElementById('btnDangNhap');

btnDangNhap.onclick = function () {
    // input: tai khoan string, matkhai: string;
    var taiKhoan = document.getElementById('dangNhap').value;
    var matKhau = document.getElementById('matKhau').value;
    // console.log(taiKhoan);
    // console.log(matKhau);/
    // output: string;
    var thongBao = '';
    thongBao = 'Tài khoản: ' + taiKhoan + '-' + 'Mật khẩu: ' + matKhau;
    document.getElementById('loGin').innerHTML = thongBao;
    document.getElementById('loGin').style.backgroundColor = 'green';
}


var tinhNao = document.getElementById('tinhNao');

tinhNao.onclick = function () {
    var thanhToan = document.getElementById('thanhToan').value;
    var phanTramTip = document.getElementById('phanTramTip').value;
    var soNguoi = document.getElementById('soNguoi').value;
    var tienTipTrenNguoi = 0;
    console.log(thanhToan);
    console.log(phanTramTip);
    console.log(soNguoi);
    tienTipTrenNguoi = (thanhToan / 100 * phanTramTip) / soNguoi;
    console.log(tienTipTrenNguoi)
    document.getElementById('tinhTip').innerHTML = '$' + tienTipTrenNguoi;
}